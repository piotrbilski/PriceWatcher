from .genericNotifier import genericNotifier
import requests

class SMSNotifier(genericNotifier):

    def login(self):
        pass


class PlaySMSNotifier(SMSNotifier):

    credentials = {}
    GATEWAY_URL = 'https://bramka.play.pl/'

    def __init__(self, username, password):
        self.credentials['username'] = username
        self.credentials['password'] = password

    def login(self):
        pass

    def set_message(self, message):
        pass

    def set_receiver(self, receiver):
        pass

    def send(self):
        pass



