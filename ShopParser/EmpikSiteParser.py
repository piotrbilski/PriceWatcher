from .GenericSiteParser import GenericSiteParser

__author__ = 'piotr'

class EmpikSiteParser(GenericSiteParser):

    def get_price(self):
        price = self.soap.find("span", {"itemprop": "price", "class": "priceSnippets"})
        return float(price.text.replace(',','.'))

    def get_description(self):
        description = self.soap.find("div", {"class": "contentPacketText longDescription"})
        return description.text.strip()

    def get_title(self):
        title = self.soap.find("span", {"itemprop": "name"})
        return title.text.strip()



