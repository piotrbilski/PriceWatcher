__author__ = 'piotr'

from bs4 import BeautifulSoup

class MethodNotImplemented(Exception):
    pass

class GenericSiteParser:
    """ Get content of site

    Parse site.

    """

    document = None
    soap = None


    def __init__(self, document):
        self.document = document
        self.soap = BeautifulSoup(document, 'html.parser')

    def get_description(self):
        raise MethodNotImplemented

    def get_price(self):
        raise MethodNotImplemented

    def get_title(self):
        raise MethodNotImplemented
