__author__ = 'piotr'

import sys
import requests
import ShopParser
import datetime

if __name__ == '__main__':

    try:
        url = sys.argv[2]
        parser = sys.argv[1]
    except:
        print("Please provide correct url address and parser")
        exit(1)

    parser_name = "%sSiteParser" % parser.capitalize()
    parser = getattr(ShopParser, parser_name, None)
    site = requests.get(url).content

    parser_object = parser(site)
    print("%s: %s - %.2f" % (datetime.datetime.now(), parser_object.get_title(),parser_object.get_price()))



