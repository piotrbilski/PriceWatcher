PriceWatcher
============
Purpose of this project is to provide simple application to monitor changes of
items prices in online shops.

First implementation supports empik.com shop but it should be easily enhanced 
for other sites.

Requirements
-------------
1. python3
2. requests
3. beautifulsoup4
